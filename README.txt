This is the ESPN_Parser written by Nate Fitzgerald.

Usage is simple, run the .jar with files to parse as arguments. The program will read those files and will display all games played from the pages organized by team. As this is just a proof of concept it does not actually do anything
with the statistics gathered, it just organizes them into an internal data structure and displays a representation of that. It should be reasonably quick, although I did not test more than 100 files simultaneously.

USAGE

java -jar ESPN_Parser filename1 URL1

or

java -jar ESPN_Parser -f outfile filename1 filename2 filename3


This program can take either a local filename or a URL to parse, and if it recieves the -f flag it will write all non-error output to a file specified as the second argument instead of standard out.


Email any questions to Nate Fitzgerald, fitzge10@augsburg.edu
