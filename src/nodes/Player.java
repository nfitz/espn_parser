/**
 * This class represents a single player in the nhl
 * @author Nate Fitzgerald
 */
package espn_parser.src.nodes;
import java.util.Hashtable;
import java.util.Map;

public class Player
{
	String name;
	Hashtable<Integer, String []> stats = new Hashtable<Integer, String []>();
	
	public Player(String name) { this.name = name; }
	
	public void addGame(int gameID, String[] gameStats) { stats.put(gameID, gameStats); }
	
	public String [] getGame(int game) { return stats.get(game); }
	
	public String getName() { return this.name; }
	
	public String toString()
	{
		StringBuilder out = new StringBuilder();
		out.append(name);
		for(int i = name.length(); i < 16; i++) out.append(" ");
		
		for (Map.Entry<Integer,String []> entry : stats.entrySet()) 
		{
		    out.append("Game: " + entry.getKey() + " ");
		    String [] stat = entry.getValue();
		    for(String s : stat) 
		    {
		    	boolean flag = false;
		    	if(s.contains("-"))
		    	{
		    		out.replace(out.length() - 2, out.length() - 1, "");
		    		flag = true;
		    	}
		    	out.append(s);
		    	for(int i = s.length(); i < 7; i++)
		    	{
		    		out.append(" ");
		    	}
		    	if(flag) out.append(" ");
		    }
		}
		return out.toString();
	}
}
