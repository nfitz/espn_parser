/**
 * This class is the wrapper class for a program to parse the boxstats nhl ESPN pages
 * @author Nate Fitzgerald
 */
package espn_parser.src.wrapper;
import espn_parser.src.parser.*;
import java.io.*;
import java.util.Scanner;
import java.lang.StringBuilder;
import java.net.URL;


public class Wrapper 
{
	public static void main(String args[])
	{
		Parser espn_parser = new Parser();

		Scanner scan1;
		String text;
		BufferedWriter write1 = null;
		if(args[0].equals("-f"))
		{
			try
			{
				File file1 = new File(args[1]);
				if (!file1.exists()) 
				{
					file1.createNewFile();
				}
				FileWriter fw = new FileWriter(file1.getAbsoluteFile());
				write1 = new BufferedWriter(fw);
			}
			catch(IOException e)
			{
				System.out.println("File writing error, check file permissions");
			}
		}

		for(int i = (write1 == null)? 0 : 2; i < args.length; i++)
		{
			if(!args[i].contains("www.") && !args[i].contains(".com"))
			{
				try
				{
					scan1 = new Scanner(new File(args[i]));
					StringBuilder sb = new StringBuilder();
					while(scan1.hasNext()) sb.append(scan1.next() + " ");
					text = sb.toString();
					scan1.close();
				}
				catch(IOException e)
				{
					System.out.println("File not found or is not readible");
					return;
				}
			}
			else text = getWebPageFromHTML(args[i]);
			
			espn_parser.parse(text);
		}
		if(write1 != null)
		{
			try
			{
				write1.write(espn_parser.toString());
				write1.close();
			}
			catch(IOException e)
			{
				System.out.println("File writing error, check file permissions");
			}
		}
		else System.out.println(espn_parser.toString());

	}
	public static String getWebPageFromHTML(String address) 
	{
	    URL url;
	    InputStream is = null;
	    BufferedReader br;
	    String line;
	    String out = "";

	    try 
	    {
	        url = new URL(address);
	        is = url.openStream();  // throws an IOException
	        br = new BufferedReader(new InputStreamReader(is));

	        while ((line = br.readLine()) != null)  out += line;
	    } 
	    catch (IOException e) 
	    {
	         e.printStackTrace();
	    }
	    finally
	    {
	        try 
	        {
	           if (is != null) is.close();
	        }
	        catch (IOException e) { }
	    }
	    return out;
	}
}
