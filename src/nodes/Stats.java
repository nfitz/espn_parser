package espn_parser.src.nodes;

public class Stats 
{
	//This stats array is all integer valued stats from left to right across the board
	int [] intstats = new int[14];
	String [] timestats = new String[4];
	//the last stat can be so easily derived from the other stats there is no reason to store it really
	
	public Stats() { }
	
	//I'm treating all stats as one cumulative index, so if you want timestat 2 you'd actually pass in 12+2=14.
	
	public void addStat(int stat, int index) { intstats[index] = stat; }
	
	public void addStat(String stat, int index) { timestats[index - 12] = stat; }
	
	public int getIntStat(int index) { return intstats[index]; }
	
	public String getTimeStat(int index) { return timestats[index - 12]; }
	
	public String toString()
	{
		String out = "";
		for(int i : intstats) out += i;
		for(String s : timestats) out += s;
		return out;
	}
}
