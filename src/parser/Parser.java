/**
 * This class is the actual parser of the ESPN_Parser, takes text in and creates Team Objects
 * @author Nate Fitzgerald
 */

package espn_parser.src.parser;
import java.util.Hashtable;
import java.util.Map;

import espn_parser.src.nodes.Team;
import espn_parser.src.nodes.Player;

public class Parser
{
	 Hashtable<String, Team> teams = new Hashtable<String, Team>();
	
	
	public Parser() { }
	
	public void parse(String text)
	{
		int gameId = getGameID(text);
		int start = text.indexOf("Player Summary");
		
		String [] teamNames = pullTeamNames(text);

		
		int summary1Start = text.indexOf("<tbody", start);
		int summary1End = text.indexOf("</tbody>", summary1Start);
		
		int nextTeamName = text.indexOf(teamNames[1], summary1End);
		
		int summary2Start = text.indexOf("<tbody", nextTeamName);
		int summary2End = text.indexOf("</tbody>", summary2Start);
		String summary1 = text.substring(summary1Start, summary1End + 8);
		String summary2 = text.substring(summary2Start, summary2End + 8);
		
		
		parseTeamStats(summary1, gameId, teamNames[0]);
		parseTeamStats(summary2, gameId, teamNames[1]);
		
	}
	
	
	/**
	 * This method parses a whole team's stats from their section of the player summary and adds them to the list of teams
	 * @param text the relevant section of the player summary
	 * @param gameId the game id, needed for adding to the players stats but never referenced within the passed text
	 * @param teamName the team's name, also not referred to within the player summary
	 */
	private void parseTeamStats(String text, int gameId, String teamName)
	{
		/**
		 * The methodology here is basically to find the table starting at the open <tr and just use split() to find everything between the tags > <, 
		 * as that will be there data. The problem is that many times there are two or more consecutive tags with nothing between them, so those
		 * must be carefully discarded.
		 */
		
		
		if(teams.get(teamName) == null)
		{
			Team team = new Team(teamName);
			teams.put(teamName, team);
		}
		Team curTeam = teams.get(teamName);

		String [] tables = text.split("<tr");

		for(int i = 0; i < tables.length; i++)
		{
			String [] stats = tables[i].split(">");
			String out = "";
			for(int j = 0; j < stats.length; j++)
			{
				if(stats[j].indexOf("<") != -1)
				{
					int stop = stats[j].indexOf("<");
					out += stats[j].substring(0, stop) + " ";
				}
			}
			String [] sts = out.split(" ");
			if(sts.length > 40)
			{
				String pname = sts[2] + " " + sts[3];
				if(!sts[4].equals("")) //This is in case of lastnames like St. Martin that have a space. This method will fail on a name with 2 spaces
									  // like de la salle but it won't throw an error, it'll just pick up the first two which should be a unique identifier anyways
				{
					pname += " " +  sts[4];
					for(int n = 3; n < sts.length - 1; n++) sts[n] = sts[n+1];
					sts[sts.length - 1] = "";
				}
				Player tplayer = new Player(pname);
				String [] tstats = new String [20];
				int counter = 0;
				for(int k = 4; k < sts.length; k++) if(!sts[k].equals("")) tstats[counter++] = sts[k];
				tplayer.addGame(gameId, tstats);

				if(curTeam.getPlayer(pname) == null) curTeam.addPlayer(tplayer);
				else curTeam.getPlayer(pname).addGame(gameId, tstats);
			}	
		}	
	}
	
	/**
	 * This method finds the winner of the game. -1 for a home team loss, 0 for a draw, and +1 for a home team win (the home team is always posted first)
	 * @param text the full or partial text of the webpage
	 * @return the winner as an integer
	 * 
	 * This class is not included at present but is potentially useful enough to be worth keeping
	 */
	private int parseWinner(String text)
	{
		int loc = text.indexOf("awayScore\">");
		int awayScore = text.indexOf(">", loc);
		
		loc = text.indexOf("homeScore\">");
		int homeScore = text.indexOf(">", loc);
		
		if(homeScore == awayScore) return 0;
		else
			return (homeScore > awayScore)? 1 : -1;
	}
	
	
	/**
	 * This method takes a string of the player summary and returns the team name of the team who's players are being summarized
	 * @param text the player summary
	 * @return the name of the teams
	 */
	private String[] pullTeamNames(String text)
	{
		String [] teamNames = new String[2];
		int title = text.indexOf("<title>");
		int endTitle = text.indexOf("</title>");
		String gameName = text.substring(title + 7, endTitle);
		int titleSplit = gameName.indexOf("vs. ");
		teamNames[0] = gameName.substring(0, titleSplit - 1);
		teamNames[1] = gameName.substring(titleSplit + 4, gameName.indexOf(" - "));
		return teamNames;
	}
	
	
	/**
	 * This method takes in the full text of the webpage and returns the integer of the game ID
	 * @param text The full text of the webpage
	 * @return the integer of the game ID
	 */
	private int getGameID(String text)
	{
		int loc = text.indexOf("gameId=");
		int end = text.indexOf("\"", loc);
		return Integer.parseInt(text.substring(loc + 7, end));
	}
	
	public String toString()
	{
		StringBuilder out = new StringBuilder();
		
		for(Map.Entry<String,Team> entry : teams.entrySet()) 
		{
		    out.append(entry.getKey() + "\n" + entry.getValue());
		}
		
		return out.toString();
	}
	

}
