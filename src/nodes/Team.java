/**
 * This class represents a single team in the nhl 
 * @author Nate Fitzgerald
 */
package espn_parser.src.nodes;
import java.util.Hashtable;
import java.util.Map;

public class Team 
{
	String name;
	
	Hashtable<String, Player> players = new Hashtable<String, Player>();
	
	
	public Team(String name) { this.name = name;}
	
	public void addPlayer(Player player)
	{ 
		if(players.get(player.getName()) == null) this.players.put(player.getName(), player);
	}

	public Player getPlayer(String name) { return players.get(name); }
	
	public String toString()
	{
		String out = "";
		for(Map.Entry<String,Player> player : players.entrySet()) 
		{
		    out += player.getValue() + "\n";
		}
		out += "\n";
		
		return out;
	}
	
}
